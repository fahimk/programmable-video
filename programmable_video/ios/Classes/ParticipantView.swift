import Flutter
import Foundation
import TwilioVideo

class ParticipantView: NSObject, FlutterPlatformView {
    private var videoView: VideoView

    private var videoTrack: VideoTrack?

    init(_ videoView: VideoView, videoTrack: VideoTrack?) {
        self.videoView = videoView
        self.videoTrack = videoTrack
        if videoTrack != nil {
            videoTrack!.addRenderer(videoView)
        }
    }

    public func view() -> UIView {
        return videoView
    }

    deinit {
        if videoTrack != nil {
            videoTrack!.removeRenderer(videoView)
        }
    }
}
